package training.kotlin.springboot.controller

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyVararg
import org.mockito.Mockito
import org.mockito.stubbing.Answer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.kafka.test.context.EmbeddedKafka
import training.kotlin.springboot.event.ChargeEvent
import training.kotlin.springboot.event.ChargeEventProducer
import training.kotlin.springboot.model.Charge
import training.kotlin.springboot.repository.ChargeHistoryRepository
import training.kotlin.springboot.repository.ChargeRepository
import training.kotlin.springboot.response.ErrorResponseObject
import training.kotlin.springboot.response.ResponseObject
import training.kotlin.springboot.response.ResponseStatus
import training.kotlin.springboot.response.SuccessResponseObject
import training.kotlin.springboot.vo.ChargeVO

@SpringBootTest
@EmbeddedKafka(partitions = 1, brokerProperties = ["listeners=PLAINTEXT://localhost:9092",
    "port=9092"])
class ControllerWithPersistentLayerTests {

    @Autowired
    lateinit var chargeController: ChargeController

    @Autowired
    lateinit var chargeRepository: ChargeRepository

    @Autowired
    lateinit var chargeHistoryRepository: ChargeHistoryRepository

    @AfterEach
    fun removeAllEntities() {
        chargeRepository.deleteAll()
    }

    @Test
    fun `Assert creating a new Charge`() {
        var chargeVO = ChargeVO()
        chargeVO.cardNo = "000001"
        chargeVO.status = "PENDING"
        chargeVO.amount = 100
        chargeVO.brand = "LV"
        chargeController.createCharge(chargeVO)

        var charges = chargeRepository.findAll()
        assert(charges.count() == 1)
    }

    @Test
    fun `Assert updating a Charge`() {
        var charge = Charge(0, "000001", "PENDING", 100,  "LV")
        chargeRepository.save(charge)

        var chargeVO = ChargeVO()
        chargeVO.chargeId = charge.chargeId
        chargeVO.cardNo = charge.cardNo
        chargeVO.status = "COMPLETED"
        chargeVO.amount = charge.amount
        chargeVO.brand = charge.brand
        chargeController.updateCharge(chargeVO, charge.chargeId)

        var chargeFromDB = chargeRepository.findByChargeId(charge.chargeId)
        assert(chargeFromDB != null)
        assert(chargeFromDB?.status == chargeVO.status)
    }

    @Test
    fun `Assert deleting a Charge`() {
        var charge = Charge(0,  "000001", "PENDING", 100, "LV")
        chargeRepository.save(charge)

        chargeController.deleteCharge(charge.chargeId)

        var chargeFromDB = chargeRepository.findByChargeId(charge.chargeId)
        assert(chargeFromDB == null)
    }

    @Test
    fun `Assert getting an existing Charge`() {
        var charge = Charge(0, "000001", "PENDING", 100, "LV")
        chargeRepository.save(charge)

        var responseObject = chargeController.getChargeByChargeId(charge.chargeId)
        assert(responseObject is SuccessResponseObject<*>)
        assert(responseObject.status == ResponseStatus.SUCCESS)

        var data = (responseObject as SuccessResponseObject<*>).data
        assert(data != null)
        assert(data is ChargeVO)

        var chargeVO = data as ChargeVO
        assert(chargeVO.chargeId == charge.chargeId)
        assert(chargeVO.cardNo == charge.cardNo)
        assert(chargeVO.status == charge.status)
        assert(chargeVO.amount == charge.amount)
        assert(chargeVO.brand == charge.brand)
    }

    @Test
    fun `Assert getting a non-existing Charge`() {
        var responseObject = chargeController.getChargeByChargeId(1)
        assert(responseObject is ErrorResponseObject)
        assert(responseObject.status == ResponseStatus.ERROR)
    }

    @Test
    fun `Assert getting all Charges`() {
        var charge = Charge(0, "000001", "PENDING", 100, "LV")
        chargeRepository.save(charge)

        charge = Charge(0, "000002", "PENDING", 100, "LV")
        chargeRepository.save(charge)

        var responseObject = chargeController.getAllCharges()
        assert(responseObject is SuccessResponseObject<*>)
        assert(responseObject.status == ResponseStatus.SUCCESS)

        var data = (responseObject as SuccessResponseObject<*>).data
        assert(data != null)
        assert(data is List<*>)
        var chargeVOs = data as List<*>
        assert(chargeVOs.count() == 2)
        for (any in chargeVOs) {
            assert(any is ChargeVO)
        }
    }
}

