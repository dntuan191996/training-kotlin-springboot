CREATE TABLE IF NOT EXISTS USER_ACCOUNT (
    USER_ID INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    USERNAME VARCHAR (50) NOT NULL,
    PASSWORD VARCHAR (50) NOT NULL,
    EMAIL VARCHAR (50) NOT NULL,
    PRIMARY KEY (USER_ID),
    UNIQUE(USERNAME),
    UNIQUE(EMAIL)
);

CREATE TABLE IF NOT EXISTS USER_ROLE (
    ROLE_ID INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    ROLE_NAME VARCHAR (50) NOT NULL,
    PRIMARY KEY (ROLE_ID),
    UNIQUE (ROLE_NAME)
);

CREATE TABLE IF NOT EXISTS USER_USER_ROLE (
    USER_ID INT NOT NULL REFERENCES USER_ACCOUNT (USER_ID) ON DELETE CASCADE,
    ROLE_ID INT NOT NULL REFERENCES USER_ROLE (ROLE_ID) ON DELETE CASCADE,
    PRIMARY KEY (USER_ID, ROLE_ID)
);

INSERT INTO USER_ROLE (ROLE_NAME)
VALUES
    ('CHARGE_READ'),
    ('CHARGE_WRITE'),
    ('CHARGE_HISTORY_READ'),
    ('CHARGE_HISTORY_WRITE')
ON CONFLICT DO NOTHING;

INSERT INTO USER_ACCOUNT (USERNAME, PASSWORD, EMAIL)
VALUES
    ('admin', 'admin', 'admin@admin.com')
ON CONFLICT DO NOTHING;

INSERT INTO USER_USER_ROLE (USER_ID, ROLE_ID)
SELECT USER_ACCOUNT.USER_ID AS USER_ID, USER_ROLE.ROLE_ID AS ROLE_ID FROM USER_ACCOUNT CROSS JOIN USER_ROLE
ON CONFLICT DO NOTHING;