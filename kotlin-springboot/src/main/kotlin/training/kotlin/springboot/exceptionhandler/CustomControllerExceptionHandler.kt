package training.kotlin.springboot.exceptionhandler

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import training.kotlin.springboot.exception.EntityNotFoundException
import training.kotlin.springboot.exception.OperationFailedException
import training.kotlin.springboot.response.ErrorResponseObject
import training.kotlin.springboot.response.ResponseStatus

@ControllerAdvice
class CustomControllerExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(
            value = [
                EntityNotFoundException::class,
                OperationFailedException::class
            ]
    )
    fun handlerControllerException(
            ex: Exception,
            request: WebRequest
    ): ResponseEntity<ErrorResponseObject> {
        var responseObject = ErrorResponseObject()

        responseObject.status = ResponseStatus.ERROR
        responseObject.message = ex.message

        val httpStatus = when (ex) {
            is EntityNotFoundException -> HttpStatus.NOT_FOUND
            is OperationFailedException -> HttpStatus.INTERNAL_SERVER_ERROR

            else -> {
                HttpStatus.INTERNAL_SERVER_ERROR
            }
        }

        return ResponseEntity<ErrorResponseObject>(responseObject, httpStatus)
    }

}