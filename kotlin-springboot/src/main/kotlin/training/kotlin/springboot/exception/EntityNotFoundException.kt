package training.kotlin.springboot.exception

import java.lang.RuntimeException

class EntityNotFoundException(
        message: String
) : RuntimeException(message)