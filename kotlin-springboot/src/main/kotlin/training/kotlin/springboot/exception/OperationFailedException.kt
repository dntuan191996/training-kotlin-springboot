package training.kotlin.springboot.exception

import java.lang.RuntimeException

class OperationFailedException(
        message: String,
        cause: Throwable?
) : RuntimeException(message, cause)