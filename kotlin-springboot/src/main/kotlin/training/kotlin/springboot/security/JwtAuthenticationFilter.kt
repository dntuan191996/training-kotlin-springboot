package training.kotlin.springboot.security

import com.auth0.jwt.JWT
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationFilter : Filter {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var jwtTokenHelper: JwtTokenHelper

    override fun doFilter(
            request: ServletRequest,
            response: ServletResponse,
            chain: FilterChain) {
        val authorization = (request as HttpServletRequest).getHeader(HttpHeaders.AUTHORIZATION)

        if (authorization != null && authorization.startsWith("Bearer ")) {
            val token = authorization.substring(7)
            SecurityContextHolder.getContext().setAuthentication(jwtTokenHelper.getAuthentication(token))
        }

        chain.doFilter(request, response)
    }

}