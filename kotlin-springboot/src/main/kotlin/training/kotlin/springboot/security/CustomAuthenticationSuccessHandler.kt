package training.kotlin.springboot.security

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class CustomAuthenticationSuccessHandler : AuthenticationSuccessHandler {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var jwtTokenHelper: JwtTokenHelper

    override fun onAuthenticationSuccess(
            request: HttpServletRequest,
            response: HttpServletResponse,
            authentication: Authentication) {

        val usernamePasswordAuthenticationToken = authentication as UsernamePasswordAuthenticationToken

        val token = jwtTokenHelper.createToken(usernamePasswordAuthenticationToken.principal.toString(),
                usernamePasswordAuthenticationToken.authorities
        )

        response.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token)

        logger.info("logged in successfully! ")

    }

}