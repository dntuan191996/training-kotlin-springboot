package training.kotlin.springboot.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class WebSecurityConfiguration : WebSecurityConfigurerAdapter() {

    @Autowired
    lateinit var authenticationSuccessHandler: CustomAuthenticationSuccessHandler

    @Autowired
    lateinit var authenticationFailureHandler: CustomAuthenticationFailureHandler

    @Autowired
    lateinit var logoutSuccessHandler: CustomLogoutSuccessHandler

    @Autowired
    lateinit var customAuthenticationProvider: CustomAuthenticationProvider

    @Autowired
    lateinit var jwtAuthenticationFilter: JwtAuthenticationFilter

    override fun configure(http: HttpSecurity) {
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .authorizeRequests()
//                .antMatchers(HttpMethod.GET,"/api/charges").hasAnyRole("CHARGE_READ", "CHARGE_WRITE")
//                .antMatchers(HttpMethod.DELETE,"/api/charges").hasRole("CHARGE_WRITE")
//                .antMatchers(HttpMethod.DELETE,"/api/charges").hasRole("CHARGE_WRITE")
//                .antMatchers(HttpMethod.DELETE,"/api/charges").hasRole("CHARGE_WRITE")
//                .antMatchers(HttpMethod.DELETE,"/api/charge_histories").hasAnyRole("CHARGE_HISTORY_READ", "CHARGE_HISTORY_WRITE")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/api/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and()
                .logout()
                .logoutUrl(("/api/logout"))
                .logoutSuccessHandler(logoutSuccessHandler)
                .invalidateHttpSession(true)
                .and()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(customAuthenticationProvider)
    }

}