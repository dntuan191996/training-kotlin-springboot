package training.kotlin.springboot.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import java.util.*
import java.util.stream.Collectors


@Component
class JwtTokenHelper {

    private val algorithm: Algorithm = Algorithm.HMAC256("secret")

    fun createToken(username: String, authorities: Collection<GrantedAuthority>): String {

        val token = JWT.create()
                .withSubject(username)
                .withArrayClaim(
                        "authorities",
                        authorities
                                .stream()
                                .map { authority -> authority.toString() }
                                .collect(Collectors.toList())
                                .toTypedArray()
                )
                .withExpiresAt(Date(System.currentTimeMillis() + 60 * 60 * 1000))
                .withIssuer("auth0")
                .sign(algorithm)
        return token
    }

    fun validateToken(token: String) {
        JWT.decode(token)
    }

    fun getAuthentication(token: String): UsernamePasswordAuthenticationToken? {
        try {
            var decodedJWT = JWT.decode(token)
            var username = decodedJWT.subject
            var authorities = decodedJWT.getClaim("authorities").asList(SimpleGrantedAuthority::class.java);

            return UsernamePasswordAuthenticationToken(username, "", authorities)
        } catch (e: Exception) {
            return null
        }
    }

}