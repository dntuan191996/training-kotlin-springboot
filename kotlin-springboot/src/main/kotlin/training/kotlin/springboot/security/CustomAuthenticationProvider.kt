package training.kotlin.springboot.security

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import training.kotlin.springboot.model.UserRole
import training.kotlin.springboot.repository.UserRepository
import java.util.stream.Collectors

@Component
class CustomAuthenticationProvider : AuthenticationProvider {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var  userRepository: UserRepository

    override fun authenticate(authentication: Authentication): Authentication {
        val username = authentication.name
        val password = authentication.credentials.toString()

        val user = userRepository.findByUsername(username)

        if (user?.password.equals(password)) {
            return UsernamePasswordAuthenticationToken(
                    username,
                    "",
                    user?.roles?.let { buildGrantedAuthorities(it) }
            )
        } else {
            throw BadCredentialsException("Invalid username or password!")
        }
    }

    override fun supports(authentication: Class<*>): Boolean {
        return UsernamePasswordAuthenticationToken::class.java
                .isAssignableFrom(authentication)
    }

    private fun buildGrantedAuthorities(roles: List<UserRole>): List<GrantedAuthority> {
        return roles
                .stream()
                .map { role -> SimpleGrantedAuthority("ROLE_" + role.roleName) }
                .collect(Collectors.toList())
    }

}