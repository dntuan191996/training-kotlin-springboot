package training.kotlin.springboot.security

enum class Role {
    CHARGE_READ,
    CHARGE_WRITE,
    CHARGE_HISTORY_READ,
    CHARGE_HISTORY_WRITE
}