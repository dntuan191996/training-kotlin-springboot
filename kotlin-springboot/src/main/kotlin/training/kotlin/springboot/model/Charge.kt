package training.kotlin.springboot.model

import javax.persistence.*

@Entity
@Table(name = "CHARGE")
data class Charge (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val chargeId: Int = 0,
    var cardNo: String,
    var status: String,
    var amount: Int,
    var brand: String
) {
    companion object {
        val ATTR_CHARGE_ID = "chargeId"
        val ATTR_CARD_NO = "cardNo"
        val ATTR_STATUS = "status"
        val ATTR_AMOUNT = "amount"
        val ATTR_BRAND = "brand"
    }
}