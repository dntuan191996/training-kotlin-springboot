package training.kotlin.springboot.model

import training.kotlin.springboot.security.Role
import javax.persistence.*

@Entity
@Table(name = "USER_ROLE")
data class UserRole (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val roleId: Int,
        @Enumerated(value = EnumType.STRING)
        val roleName: Role
)