package training.kotlin.springboot.model

import javax.persistence.*

@Entity
@Table(name = "USER_ACCOUNT")
data class User(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val userId: Int = 0,
        val username: String,
        var password: String,
        var email: String,
        @ManyToMany(fetch = FetchType.EAGER)
        @JoinTable(
                name = "USER_USER_ROLE",
                joinColumns = arrayOf(JoinColumn(name = "USER_ID")),
                inverseJoinColumns = arrayOf(JoinColumn(name = "ROLE_ID"))
        )
        var roles: List<UserRole>
)