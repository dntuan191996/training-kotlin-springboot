package training.kotlin.springboot.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import training.kotlin.springboot.model.User

@Repository
interface UserRepository : CrudRepository<User, Int> {

    fun findByUsername(username: String): User?
    fun findByUserId(id: Int): User?

}