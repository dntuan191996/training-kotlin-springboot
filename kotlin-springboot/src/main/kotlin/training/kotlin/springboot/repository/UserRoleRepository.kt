package training.kotlin.springboot.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import training.kotlin.springboot.model.UserRole

@Repository
interface UserRoleRepository : CrudRepository<UserRole, Int> {

}