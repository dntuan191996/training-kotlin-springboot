package training.kotlin.springboot.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import training.kotlin.springboot.model.Charge
import java.util.*

@Repository
interface ChargeRepository: CrudRepository<Charge, Int> {

    fun findByChargeId(chargeId: Int): Charge?

}