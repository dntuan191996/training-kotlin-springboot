package training.kotlin.springboot.vo

import training.kotlin.springboot.model.User
import training.kotlin.springboot.model.UserRole
import java.util.stream.Collectors

data class UserVO (
        var userId: Int = 0,
        var username: String,
        var password: String,
        var email: String,
        var roles: List<UserRoleVO>
)

fun UserVO.convertToUser() = User(
        username = username,
        password = password,
        email = email,
        roles = roles.stream().map{ role -> role.convertToUserRole()}.collect(Collectors.toList())
)