package training.kotlin.springboot.vo

import training.kotlin.springboot.model.UserRole
import training.kotlin.springboot.security.Role

data class UserRoleVO (
        val roleId: Int,
        val roleName: Role
)

fun UserRoleVO.convertToUserRole() = UserRole(
        roleId = roleId,
        roleName = roleName
)