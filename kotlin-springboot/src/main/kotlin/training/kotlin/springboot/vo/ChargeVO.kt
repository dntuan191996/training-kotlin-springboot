package training.kotlin.springboot.vo

import training.kotlin.springboot.model.Charge

class ChargeVO {
    var chargeId: Int? = null
    lateinit var cardNo: String
    lateinit var status: String
    var amount: Int = 0
    lateinit var brand: String

    fun toCharge() = Charge(0, cardNo, status, amount, brand)
}

