package training.kotlin.springboot.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import training.kotlin.springboot.repository.ChargeHistoryRepository
import training.kotlin.springboot.response.ErrorResponseObject
import training.kotlin.springboot.response.ResponseObject
import training.kotlin.springboot.response.ResponseStatus
import training.kotlin.springboot.response.SuccessResponseObject

@RestController
@RequestMapping("/api/charge_histories")
class ChargeHistoryController : AbstractController() {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var chargeHistoryRepository: ChargeHistoryRepository

    @GetMapping()
    fun searchChargeHistories(): ResponseObject {
        return try {
            buildSuccessResponse(
                    chargeHistoryRepository.findAll(),
                    "Charge histories have been retrieved successfully!"
            )
        } catch (e: Exception) {
            logger.error("Failed to search charge histories!", e)
            buildErrorResponse("Failed to search charge histories!")
        }
    }
}