package training.kotlin.springboot.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable
import training.kotlin.springboot.event.ChargeEvent
import training.kotlin.springboot.event.ChargeEventAction
import training.kotlin.springboot.event.ChargeEventProducer
import training.kotlin.springboot.exception.EntityNotFoundException
import training.kotlin.springboot.exception.OperationFailedException
import training.kotlin.springboot.model.Charge
import training.kotlin.springboot.repository.ChargeRepository
import training.kotlin.springboot.response.ErrorResponseObject
import training.kotlin.springboot.response.ResponseObject
import training.kotlin.springboot.response.ResponseStatus
import training.kotlin.springboot.response.SuccessResponseObject
import training.kotlin.springboot.utils.ChargeVOUtils
import training.kotlin.springboot.vo.ChargeVO
import java.util.stream.Collector
import java.util.stream.Collectors
import kotlin.streams.asStream

@RestController
@RequestMapping("/api/charges")
class ChargeController : AbstractController() {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var chargeRepository: ChargeRepository

    @Autowired
    lateinit var chargeEventProducer: ChargeEventProducer

    @GetMapping("")
    fun getAllCharges(): ResponseObject {
        logger.info("get all charges")

        var searchResult = chargeRepository.findAll()
        var chargeVOs = searchResult.asSequence().asStream().map { charge -> ChargeVOUtils.convertChargeToChargeVO(charge) }.collect(Collectors.toList())
        return buildSuccessResponse(
                chargeVOs,
                "All entities have been retrieved successfully!"
        )
    }

    @GetMapping("/{chargeId}")
    fun getChargeByChargeId(@PathVariable("chargeId") chargeId: Int): ResponseObject {
        logger.info("get charge by chargeId")

        val charge = chargeRepository.findByChargeId(chargeId)
                ?: throw EntityNotFoundException("The entity doesn't exist!")

        return buildSuccessResponse(ChargeVOUtils.convertChargeToChargeVO(charge), "The entity has been retrieved successfully!")
    }

    @PostMapping
    fun createCharge(@RequestBody chargeVO: ChargeVO): ResponseObject {
        logger.info("create charge")
        var charge = chargeVO.toCharge()

        chargeRepository.save(charge)
        chargeVO.chargeId = charge.chargeId

        chargeEventProducer.send(charge, ChargeEventAction.CREATE)

        return buildSuccessResponse(chargeVO, "The entity has been created successfully!")
    }

    @PutMapping("/{chargeId}")
    fun updateCharge(@RequestBody chargeVO: ChargeVO, @PathVariable("chargeId") chargeId: Int): ResponseObject {
        logger.info("update charge")

        val charge = chargeRepository.findByChargeId(chargeId)
                ?: throw EntityNotFoundException("The entity doesn't exist!")

        charge.apply {
            cardNo = chargeVO.cardNo
            amount = chargeVO.amount
            status = chargeVO.status
            brand = chargeVO.brand
        }

        chargeRepository.save(charge)
        chargeEventProducer.send(charge, ChargeEventAction.UPDATE)

        return buildSuccessResponse(chargeVO, "The entity has been updated successfully!")
    }

    @DeleteMapping("/{chargeId}")
    fun deleteCharge(@PathVariable("chargeId") chargeId: Int): ResponseObject {
        logger.info("delete charge")
        val charge = chargeRepository.findByChargeId(chargeId)

        charge?.let {
            chargeRepository.delete(it)
            chargeEventProducer.send(it, ChargeEventAction.DELETE)
        }

        return buildSuccessResponse(null, "The entity has been deleted successfully!")
    }

}