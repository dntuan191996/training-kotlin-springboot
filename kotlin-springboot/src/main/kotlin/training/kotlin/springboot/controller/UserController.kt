package training.kotlin.springboot.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import training.kotlin.springboot.event.ChargeEvent
import training.kotlin.springboot.event.ChargeEventAction
import training.kotlin.springboot.exception.EntityNotFoundException
import training.kotlin.springboot.repository.UserRepository
import training.kotlin.springboot.repository.UserRoleRepository
import training.kotlin.springboot.response.ResponseObject
import training.kotlin.springboot.utils.ChargeVOUtils
import training.kotlin.springboot.vo.ChargeVO
import training.kotlin.springboot.vo.UserVO
import training.kotlin.springboot.vo.convertToUser
import training.kotlin.springboot.vo.convertToUserRole
import java.util.stream.Collectors

@RestController
@RequestMapping("/api/users")
class UserController : AbstractController() {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var userRoleRepository: UserRoleRepository

    @GetMapping("/roles")
    fun getAllUserRoles(): ResponseObject {
        return buildSuccessResponse(
                userRoleRepository.findAll(),
                "All user roles have been retrieved successfully!"
        )
    }

    @GetMapping("")
    fun getAllUsers(): ResponseObject {
        return buildSuccessResponse(
                userRepository.findAll(),
                "All users have been retrieved successfully!"
        )
    }

    @PostMapping
    fun createUser(@RequestBody userVO: UserVO): ResponseObject {
        logger.info("create user")
        var user = userVO.convertToUser()

        userRepository.save(user)
        userVO.userId = user.userId
        return buildSuccessResponse(userVO, "The entity has been created successfully!")
    }

    @PutMapping
    fun updateUser(@RequestBody userVO: UserVO): ResponseObject {
        logger.info("update user")

        val user = userRepository.findByUserId(userVO.userId)
                ?: throw EntityNotFoundException("The entity doesn't exist!")

        user.apply {
            password = userVO.password
            email = userVO.email
            roles = userVO.roles.stream().map { role -> role.convertToUserRole() }.collect(Collectors.toList())
        }
        userRepository.save(user)
        return buildSuccessResponse(userVO, "The entity has been updated successfully!")
    }

    @DeleteMapping("/{userId}")
    fun deleteUser(@PathVariable("userId") userId: Int): ResponseObject {
        logger.info("delete user")
        val user = userRepository.findByUserId(userId)

        user?.let {
            userRepository.delete(it)
        }

        return buildSuccessResponse(null, "The entity has been deleted successfully!")
    }
}