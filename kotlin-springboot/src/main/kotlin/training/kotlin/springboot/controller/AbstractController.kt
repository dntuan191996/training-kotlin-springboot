package training.kotlin.springboot.controller

import training.kotlin.springboot.response.ErrorResponseObject
import training.kotlin.springboot.response.ResponseObject
import training.kotlin.springboot.response.ResponseStatus
import training.kotlin.springboot.response.SuccessResponseObject

open class AbstractController  {


    fun <T> buildSuccessResponse(data: T?, message: String): ResponseObject {
        var responseObject = SuccessResponseObject<T>()

        responseObject.status = ResponseStatus.SUCCESS
        responseObject.data = data
        responseObject.message = message

        return responseObject
    }


    fun buildErrorResponse(errorMessage: String): ResponseObject {
        var responseObject = ErrorResponseObject()

        responseObject.status = ResponseStatus.ERROR
        responseObject.message = errorMessage

        return responseObject
    }

}