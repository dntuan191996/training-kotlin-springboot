package training.kotlin.springboot.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.avro.generic.GenericRecord
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component
import training.kotlin.springboot.model.Charge
import training.kotlin.springboot.model.ChargeHistory
import training.kotlin.springboot.repository.ChargeHistoryRepository
import java.math.BigInteger
import java.sql.Timestamp
import java.time.LocalDateTime

@Component
class ChargeEventConsumer {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val jsonMapper = ObjectMapper().registerModule(KotlinModule())

    @Autowired
    private lateinit var chargeHistoryRepository: ChargeHistoryRepository

    @KafkaListener(topics = ["charge_events"], groupId = "group_1")
    fun processMessage(chargeEvent: ChargeEvent) {
        var chargeHistory = ChargeHistory(
                chargeHistoryId = 0,
                content = jsonMapper.writeValueAsString(chargeEvent.getChargeObject()),
                createdTime = Timestamp.valueOf(LocalDateTime.now()))
        chargeHistoryRepository.save(chargeHistory)
    }

}