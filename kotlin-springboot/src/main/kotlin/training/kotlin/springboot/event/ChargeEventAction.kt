package training.kotlin.springboot.event

enum class ChargeEventAction {
    CREATE, UPDATE, DELETE
}