package training.kotlin.springboot.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.avro.generic.GenericRecordBuilder
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import training.kotlin.springboot.model.Charge
import java.io.File

@Component
class ChargeEventProducer(private val kafkaTemplate: KafkaTemplate<String, ChargeEvent>) {

    fun send(charge: Charge, action: ChargeEventAction) {
        val chargeEvent = ChargeEvent(
                action.name,
                System.currentTimeMillis(),
                ChargeObject(
                        charge.chargeId,
                        charge.cardNo,
                        charge.status,
                        charge.amount,
                        charge.brand
                )
        )
        kafkaTemplate.send("charge_events", chargeEvent)
    }

}