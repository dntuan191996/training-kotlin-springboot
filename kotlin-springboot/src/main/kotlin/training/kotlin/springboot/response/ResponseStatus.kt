package training.kotlin.springboot.response

enum class ResponseStatus {
    SUCCESS, ERROR
}