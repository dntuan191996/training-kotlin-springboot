package training.kotlin.springboot.response

class SuccessResponseObject<T>: ResponseObject() {
    var data: T? = null
}