package training.kotlin.springboot.response

open class ResponseObject {
    var status: ResponseStatus? = null
    var message: String? = null
}